# Unity Template #

This project is a template for every Theoriz Unity project. 

### What does it contains ? ###

- *Scene Manager* which allows you to switch easily between scenes by simply adding them to Unity build settings.
- *OSCControlFramework* : see https://github.com/benkuper/Unity-OSCControlFramework
- *SharedTexture* : Spout or Funnel. Make your camera render inside *Spout-RenderTexture* and you are good (You might also want to add InvertCamera script) !
- *FPSDisplay* : shows actual FPS (Add it to your camera if you want it)

### How do I get set up? ###

* Make a copy of it
* Create a folder with your project name
* Create a new scene
* Create a "SceneName"MrScene script which extends MrScene and put it in the root object of your scene
* Add it to Unity build settings
* Run *Core* scene !

### Controls ###

Press "h"  to toggle UI
Press "f" to toggle FPSDisplay

### More informations ###
There is an example for OSCControlFramework in Cube scene.