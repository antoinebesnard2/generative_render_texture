﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCControlExample : Controllable
{
    [OSCProperty]
    [Range(-100.0f, 100.0f)]
    public float Speed;

    [OSCProperty] public bool Hardcore;

    public GameObject HardcoreObject;
    public Texture2D debugTexture;
    public Texture2D realTexture;
    public Rotate rotateScrip;

    public override void Awake()
    {
        usePanel = true;
        debug = false;
        base.Awake();
        Speed = 40.0f;
    }

    public override void Update()
    {
        base.Update();
        rotateScrip.Speed = Speed;
        
        HardcoreObject.SetActive(Hardcore);
    }

    [OSCMethod]
    public void SetDebugTexture()
    {
         GetComponent<Renderer>().material.mainTexture = debugTexture;
    }

    [OSCMethod]
    public void SetClassicTexture()
    {
        GetComponent<Renderer>().material.mainTexture = realTexture;
    }
}
