﻿using UnityEngine;

namespace GenerativeTextures
{
    public class CustomRenderTextureUpdater : MonoBehaviour
    {
        [SerializeField] CustomRenderTexture _texture;
        [SerializeField, Range(0, 16)] public int _stepsPerFrame = 4;

        void Start()
        {
            _texture.Initialize();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                _stepsPerFrame++;
            }
            else if(Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                _stepsPerFrame--;
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                _texture.Initialize();
            }
            if (_stepsPerFrame >= 0)
            {
                _texture.material.SetPass(0);
            }
            else
            {
                _texture.material.SetPass(1);
            }
            _texture.Update(Mathf.Abs(_stepsPerFrame));
        }
    }
}
