Shader "GenerativeTexture/ReactionDiffusion/Seeders/Triangles"
{
    Properties
    {
        _Seed("Seeding", Range(0, 1)) = 0
    }

    CGINCLUDE

    #include "UnityCustomRenderTexture.cginc"

    half _Seed;

	float2 truchetPatern(float2 st, float index)
	{
		index = frac(((index - 0.5)*2.0));
		if (index > 0.75)
		{
			st = float2(1.0,1.0) - st;
		}
		else if (index > 0.5)
		{
			st = float2(1.0 - st.x, st.y);
		}
		else if (index > 0.25) {
			st = 1.0 - float2(1.0 - st.x, st.y);
		}
		return st;
	}

    float Random(float2 uv)
    {
        return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453);
    }

    half4 frag(v2f_init_customrendertexture i) : SV_Target
    {
		float2 st = i.texcoord * 20;

		float2 ipos = floor(st);
		float2 fpos = frac(st);

		float2 tile = truchetPatern(fpos, Random(ipos * _Seed));
		float color = 0.0;
		//color = smoothstep(tile.x - 0.3, tile.x, tile.y) - smoothstep(tile.x, tile.x + 0.3, tile.y);
		color = step(tile.x, tile.y);
        return half4(half3(1 - color ,color,0), 1.0);
    }

    ENDCG

    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            Name "Init"
            CGPROGRAM
            #pragma vertex InitCustomRenderTextureVertexShader
            #pragma fragment frag
            ENDCG
        }
    }
}
