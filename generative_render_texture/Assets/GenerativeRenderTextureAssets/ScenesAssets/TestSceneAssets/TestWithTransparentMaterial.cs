﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWithTransparentMaterial : MonoBehaviour {
    public List<GameObject> PlanesList;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");
        int i = 0;
        while (i < PlanesList.Count)
        {
            PlanesList[i].GetComponent<Transform>().position += new Vector3(x, 0, y) * ((1 + i) * 0.01f); 
            i++;
        }
    }
}
