﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ControlScript : Controllable
{
    [OSCProperty] public bool Recording = false;
    [HideInInspector] public List<string> FormatList = Enum.GetNames(typeof(UTJ.FrameCapturer.MovieEncoder.Type)).ToList();
    [OSCProperty] public bool PlayingTheScene = false;
    public GenerativeTextures.CustomRenderTextureUpdater SceneUpdater;
    [OSCProperty(TargetList = "FormatList")] public string SelectedFormat;
    [Range(0, 20)]
    [OSCProperty]
    public float StepPerFrame = 5f;
    [OSCProperty] public int StartFrame;
    [OSCProperty] public int EndFrame = 100;
    [OSCProperty] public int TargetFrameRate = -1;
    int _frameCount = 0;
    bool RecordStatus = false;
    public override void Awake()
    {
        Application.targetFrameRate = TargetFrameRate;
        SelectedFormat = FormatList[0];
        GetComponent<UTJ.FrameCapturer.MovieRecorder>().m_encoderConfigs = new UTJ.FrameCapturer.MovieEncoderConfigs((UTJ.FrameCapturer.MovieEncoder.Type)Enum.Parse(typeof(UTJ.FrameCapturer.MovieEncoder.Type), SelectedFormat));
        SceneUpdater._stepsPerFrame = 0;
        usePanel = true;
        debug = false;
        RecordStatus = false;
        _frameCount = 0;
        StepPerFrame = 5f;
        base.Awake();
    }

    public override void Update()
    {
        if (PlayingTheScene)
        {
            SceneUpdater._stepsPerFrame = Mathf.FloorToInt(StepPerFrame);
            if (_frameCount > StartFrame && (_frameCount < EndFrame || EndFrame == -1))
            {
                if (Recording && !RecordStatus)
                {
                    GetComponent<UTJ.FrameCapturer.MovieRecorder>().m_encoderConfigs = new UTJ.FrameCapturer.MovieEncoderConfigs((UTJ.FrameCapturer.MovieEncoder.Type)Enum.Parse(typeof(UTJ.FrameCapturer.MovieEncoder.Type), SelectedFormat));
                    GetComponent<UTJ.FrameCapturer.MovieRecorder>().BeginRecording();
                    RecordStatus = true;
                }
                else if (!Recording && RecordStatus)
                {
                    GetComponent<UTJ.FrameCapturer.MovieRecorder>().EndRecording();
                    RecordStatus = false;
                }
            }
            if (_frameCount > StartFrame)
                StartFrame = _frameCount;
            _frameCount++;
        }
        else
        {
            GetComponent<UTJ.FrameCapturer.MovieRecorder>().EndRecording();
            RecordStatus = false;
            SceneUpdater._stepsPerFrame = 0;
            if (TargetFrameRate != Application.targetFrameRate)
                Application.targetFrameRate = TargetFrameRate;
        }
        base.Update();

    }
}

